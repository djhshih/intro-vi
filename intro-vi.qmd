---
title: "Primer on Variation Inference"
author: "David J. H. Shih"
date: last-modified
pdf-engine: pdflatex
format:
  pdf
---


## Statistical inference

Given data $x$ and parameter $\theta$

In classical statistics, we examine the likelihood
$$
p\left(x \mid \theta\right)
$$
for variable $x$ and fixed $\theta$.

In Bayesian statistics, we examine the posterior distribution
$$
p\left(\theta \mid x\right) = 
\frac{p\left(x \mid \theta \right) p\left(\theta\right)}
{p\left(x\right)}
$$
for fixed $x$ and variable $\theta$.

## Central Bayesian challenge

The evidence can be difficult to evaluate because integration is hard!
$$
p\left( x \right) =
\int_{\theta \in \Theta} p\left(x \mid \theta \right) p\left(\theta \right)
$$

## Markov chain Monte Carlo

Use Metropolis-Hastings sampling to draw a sample of the posterior
$$
p\left(\theta \mid x\right) \propto p\left(x \mid \theta \right) p\left(\theta\right)
$$

**Advantage**: constant $p\left(x\right)$ is not needed

**Disadvantage**: slow, non-scalable

## Variational inference

Approximate $p\left(\theta \mid x\right)$ with a density function
$q\left(\theta\right)$
$$
q^*(\theta) = \textrm{arg min}_{q(\theta) \in \mathscr{Q}}
\textrm{KL} \left( q(\theta) \; \Vert \; p(\theta \mid x)  \right)
$$

Because the KL is not computable, we optimize an alternative objective
$$
\textrm{ELBO}\left( q \right) \triangleq
\mathbb{E}_q\left[ \textrm{log} \, p(\theta, x) \right]
- \mathbb{E}_q\left[ \textrm{log} \, q(\theta) \right]
$$

We see that the ELBO is proportional to the KL
$$
\begin{align}
\textrm{KL} \left( q(\theta) \; \Vert \; p(\theta \mid x) \right)
&= \mathbb{E}_q\left[ \textrm{log} \, q(\theta) \right]
- \mathbb{E}_q\left[ \textrm{log} \, p(\theta \mid x) \right] \\
&= \mathbb{E}_q\left[ \textrm{log} \, q(\theta) \right]
- \mathbb{E}_q\left[ \textrm{log} \, p(\theta , x) \right]
+ \mathbb{E}_q\left[ \textrm{log} \, p(x) \right] \\
&= -\textrm{ELBO}\left( q \right)
+ \mathbb{E}_q\left[ \textrm{log} \, p(x) \right] \\
&\propto -\textrm{ELBO}\left( q \right)
\end{align}
$$

## Definitions

Expectation
$$
\mathbb{E}_q \left[ f(x) \right] \triangleq \int q(x) f(x) \, dx
$$

Kullback-Leibler divergence
$$
\begin{align}
\textrm{KL} \left( q(x) \; \Vert \; p(x) \right)
&\triangleq \int q(x) \, \textrm{log}\left( \frac{ q(x) }{ p(x) } \right) \, dx \\
&= \int q(x) \, \textrm{log} \, q(x) - q(x) \, \textrm{log} \, p(x) \, dx \\
&= \mathbb{E}_q \left[ \textrm{log} \, q(x) \right] - 
\mathbb{E}_q \left[ \textrm{log} \, p(x) \right]
\end{align}
$$

